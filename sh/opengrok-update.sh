#!/bin/bash

BASE=~/opengrok-src

for dir in $BASE/*/
do
    cd ${dir}
    git pull
done

docker exec OpenGrok /opengrok-0.12.1.5/bin/OpenGrok index /src
